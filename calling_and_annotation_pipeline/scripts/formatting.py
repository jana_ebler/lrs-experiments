import sys
import pandas as pd
from collections import defaultdict

instructions_file = sys.argv[1]
table_file = sys.argv[2]
sample_name = sys.argv[3]
outname = sys.argv[4]

filename_to_sample = {
	"137-3" : "KB0122_C",
	"137-4" : "KB0192_C",
	"m54274Ue_211017_003117.subreads" : "KB0279_C",
	"m54274Ue_211018_064300.subreads" : "KB0241_C",
	"m54274Ue_211019_125500.subreads" : "KB0056_C",
	"m54274Ue_211026_184129.subreads" : "KB0230_C",
	"KB0155_tumor" : "KB0155_tumor", 
	"KB0279_tumor" : "KB0279_tumor", 
	"KB0365_tumor" : "KB0365_tumor",
	"KB0427_tumor" : "KB0427_tumor",
	"m64093_210121_104318.subreads" : "KB0122_C",
	"m64093_220302_162900.subreads": "KB0025_C",
	"m54274Ue_220301_015441.subreads": "KB0330_C", 
	"m54274Ue_220314_180538.subreads": "KB0240_C",
	"m64093_220406_164627.subreads" : "KB0025_tumor",
	"m64093_220408_001316.subreads": "KB0427_C",
	"m64093_220409_091254.subreads": "KB0365_C",
	"m64093_220410_181236.subreads": "KB0330_tumor",
	"m64093_220327_111918.subreads": "KB0016_tumor",
	"m64093_220326_021759.subreads": "KB0145_C",
	"449-50": "KB0159_tumor",

	 "m64093_220615_095016.subreads" : "KB0053_tumor",
	 "m64093_220519_073350.subreads" : "KB0043_C",
	 "m64093_220520_144644.subreads" : "MHH-CALL2",
	 "m64093_220521_215937.subreads" : "KB0270_C",
	 "m64093_220523_051614.subreads" : "KB0042_C",
	 "m64093_220616_170604.subreads" : "KB0233_C",
	 "m64093_220618_020551.subreads" : "REH",
	 "m64093_220619_110607.subreads" : "KB0265_C",

	 "m64093_220811_144426.subreads" : "KB0043_tumor",
	 "m64093_220812_220738.subreads" : "KB0122_tumor",
	 "m64093_220814_051930.subreads" : "KB0306_C",
	 "m64093_220815_124237.subreads" : "KB0234_C",
	 "m64093_220816_195636.subreads" : "KB0468_C",
	 "m64093_220818_030721.subreads" : "KB0016_C", 
	 "m64093_220819_103014.subreads" : "KB0408_C",

	"m64093_220728_072514.subreads" : "KB0230_tumor",
	"m64093_221024_233204.subreads" : "KB0215_C",
	"m54274Ue_220927_153322.subreads": "KB0468_tumor",
	"m64093_221108_180851.subreads" : "KB0056_tumor",
	"m54274Ue_221023_135612.subreads" : "KB0363_C",
	"m54274Ue_220928_225320.subreads" : "KB0215_tumor",
	"m64093_221110_013508.subreads" : "KB0241_tumor",
	"m54274Ue_220930_060942.subreads" : "KB0294_tumor",
	"m54274Ue_221001_133159.subreads" : "KB0159_C",
	"m64093_221111_085958.subreads" : "KB0093_tumor",
	"m64093_221112_162500.subreads" : "KB0408_tumor",
	"m64093_221202_152023.subreads" : "KB0053_C",
	"m64093_221203_222920.subreads" : "KB0294_C",
	"m54274Ue_221203_223450.subreads" : "KB0111_tumor",
	"m54274Ue_221202_152445.subreads" : "K7_tumor"

}

old_to_new = {}
new_order = []
with open(instructions_file) as infile:
	instructions = [ [f for f in next(infile).strip().split('\t')] for x in range(2)]
	print(instructions)
	new_order = []
	for new,old in zip(instructions[0], instructions[1]):
		if new == "":
			continue
		if "_filtered" in old:
			new_order.append(filename_to_sample[sample_name] + '_filtered')
			old_to_new[sample_name + '_filtered'] = filename_to_sample[sample_name] + '_filtered'
			continue
		new_order.append(new)
		if old.startswith('in_'):
			continue
		if old.startswith('ID_'):
			continue
		old_to_new[old] = new
print(old_to_new)
print(new_order)

old_to_new['in_pbsv_' + sample_name] = 'pbsv'
old_to_new['in_sniffles_' + sample_name] = 'sniffles'
old_to_new['in_svim_' + sample_name] = 'svim'
old_to_new['in_hgsvc'] = 'in_HGSVC'

old_to_new['ID_pbsv_' + sample_name] = 'ID_pbsv'
old_to_new['ID_sniffles_' + sample_name] = 'ID_sniffles'
old_to_new['ID_svim_' + sample_name] = 'ID_svim'
old_to_new['ID_hgsvc'] = 'ID_HGSVC'

# read table
df = pd.read_csv(table_file, sep='\t')


print(df)

# rename columns
df.rename(columns=old_to_new, inplace=True)

print(df)

# keep only columns in old_to_new
df.to_csv(outname, columns=new_order, sep='\t', index=False, na_rep='nan')
