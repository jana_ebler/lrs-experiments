import sys, argparse
from collections import defaultdict
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages
import numpy as np
import upsetplot
import pandas as pd

if __name__ == '__main__':
	parser = argparse.ArgumentParser(prog='plot_upset.py', description=__doc__)
	parser.add_argument('-t', '--table', required=True, help='Table to which annotations shall be added.')
	parser.add_argument('-o', '--output', required=True, help='output name prefix.')
	parser.add_argument('-n', '--names', nargs='+', default=[], required=True, help='Names of columns to include in plot.')
	parser.add_argument('-r', '--regions', nargs='+', default=[], help='For each region, plot variants in/outside that region.')
	args = parser.parse_args()
	
	df = pd.read_csv(args.table, sep='\t')
	df = df[df[args.names].any(1)]

	with PdfPages(args.output) as pdf:
		labels = [a[3:] for a in args.regions]
		in_regions = []
		out_regions = []
		for r in args.regions:
			plt.figure()
			in_region = len(df[df[r]])
			out_region = len(df[~df[r]])
			assert in_region + out_region == len(df)
			in_regions.append(in_region)
			out_regions.append(out_region)
			print('in ' + r + ': ' + str(in_region))
			print('outside ' + r + ': ' + str(out_region))
		
		x = np.arange(len(labels))  # the label locations
		width = 0.2  # the width of the bars
		fig, ax = plt.subplots()
		rects1 = ax.bar(x - width/2, in_regions, width, label='in callset/region')
		rects2 = ax.bar(x + width/2, out_regions, width, label='not in callset/region')

		ax.set_ylabel('number of variants')
		ax.set_title('variants in/outside of callsets/regions')
		ax.set_xticks(x)
		ax.set_xticklabels(labels, rotation=90)
		ax.set_yscale('log')
		ax.legend()
		plt.tight_layout()
		pdf.savefig()
		plt.close()
