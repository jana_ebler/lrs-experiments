#!/bin/sh

coverage=$1
threads=$2
input=$3
output=$4
log=$5
value=`cat $coverage`
threshold=$(( value / 3 ))
adjusted=$(( threshold < 3 ? 3 : threshold ))
echo "$adjusted"
sniffles -t $threads -m $input -v $output -s $adjusted &>> $log
