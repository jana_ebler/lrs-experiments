import sys, argparse
from collections import defaultdict
import pandas as pd

def mean(values):
	numbers = [float(v) for v in values if v != 'nan']
	if len(numbers) == 0:
		return 'nan'
	return str(float(sum(numbers)) / max(len(numbers), 1))

if __name__ == '__main__':
	parser = argparse.ArgumentParser(prog='add_bed_overlaps.py', description=__doc__)
	parser.add_argument('-t', '--table', required=True, help='Table to which annotations shall be added.')
	parser.add_argument('-i', '--intersections', required=True, nargs='+', help='bedtools intersect output for two VCF files (keeping both entries)')
	parser.add_argument('-o', '--output', required=True, help='output table with added columns.')
#	parser.add_argument('-n', '--name', default='intersection', nargs='+', help='name of the callset that was intersected.')
	args = parser.parse_args()

	df = pd.read_csv(args.table, sep='\t')
	# add table indicating overlap
	for filename in args.intersections:
		column_name = '_'.join(filename.split('/')[-1][:-4].split('_')[1:])
		callset_name = filename.split('/')[-1][:-4]
		if callset_name.startswith('closest-'):
			intersections_20mb = defaultdict(list)
			intersections_10kb = defaultdict(list)
			for line in open(filename, 'r'):
				fields = line.split('\t')
				id_first = fields[2].strip()
				id_second = fields[11].strip()
				distance = fields[-1].strip()
				if id_second != ".":
					if abs(int(distance)) < 20000000:
						intersections_20mb[id_first].append(id_second + '(' + distance + 'bp)')
					if abs(int(distance)) < 10000:
						intersections_10kb[id_first].append(id_second + '(' + distance + 'bp)')
			closest_20mb = []
			closest_10kb = []
			for var in df['ID']:
				if var in intersections_20mb:
					feature = ';'.join(list(set(intersections_20mb[var])))
					assert len(feature) > 1
					closest_20mb.append(';'.join(list(set(intersections_20mb[var]))))
				else:
					closest_20mb.append('nan')
			for var in df['ID']:
				if var in intersections_10kb:
					feature = ';'.join(list(set(intersections_10kb[var])))
					assert len(feature) > 1
					closest_10kb.append(';'.join(list(set(intersections_10kb[var]))))
				else:
					closest_10kb.append('nan')
			if 'downstream' in filename:
				direction = 'DOWNSTREAM'
			else:
				assert 'upstream' in filename
				direction = 'UPSTREAM'
			df['CLOSEST-' + direction + '<20mb_' + column_name] = closest_20mb
			df['CLOSEST-' + direction + '<10kb_' + column_name] = closest_10kb
		elif callset_name.startswith('variants_'):
			intersections = defaultdict(list)
			allele_frequencies = defaultdict(list)
			for line in open(filename, 'r'):
				fields = line.split('\t')
				id_first = fields[2].strip()
				id_second = fields[11].strip()
				assert id_second != '.'
				allele_freq = 'nan'
				if 'AF_' in id_second:
					splitted = id_second.split('_')
					assert 'AF' in splitted
					af_index = splitted.index('AF') + 1
					allele_freq = float(splitted[af_index])
				allele_frequencies[id_first].append(allele_freq)
				intersections[id_first].append(id_second)
			variants = []
			frequencies = []
			for var in df['ID']:
				if var in intersections:
					feature = ';'.join(list(set(intersections[var])))
					assert len(feature) > 1
					variants.append(feature)
					frequencies.append(mean(allele_frequencies[var]))
				else:
					frequencies.append('nan')
					variants.append('nan')
			df['VARIANT-OVERLAP_' + column_name] = variants
			if (any([f != 'nan' for f in frequencies])):
				df['AF_' + column_name] = frequencies
		elif callset_name.startswith('regions_'):
			intersections = defaultdict(list)
			for line in open(filename, 'r'):
				fields = line.split('\t')
				start_first = int(fields[1])
				info_fields = {d.split('=')[0] : d.split('=')[1] for d in fields[7].split(';') if '=' in d}
				end_first = int(info_fields['END'])
				id_first = fields[2].strip()
				start_second = int(fields[9])
				end_second = int(fields[10])
				id_second = fields[11].strip()
				assert id_second != '.'
				overlaps_left = False
				overlaps_right = False
				inside_variant = False
				if (start_second <= start_first):
					overlaps_left = True
				if (end_second >= end_first):
					overlaps_right = True
				if (start_second > start_first) and (end_second < end_first):
					inside_variant = True
				assert overlaps_left or overlaps_right or inside_variant
				intersections[id_first].append([id_second, overlaps_left, overlaps_right, inside_variant])
			left_bp = []
			right_bp = []
			inside = []
			for var in df['ID']:
				if var in intersections:
					assert len(intersections[var]) > 0
					features_left = ';'.join(list(set([stats[0] for stats in intersections[var] if stats[1]])))
					features_right = ';'.join(list(set([stats[0] for stats in intersections[var] if stats[2]])))
					features_inside = ';'.join(list(set([stats[0] for stats in intersections[var] if stats[3]])))
				
					left_bp.append(features_left if len(features_left) > 0 else 'nan')
					right_bp.append(features_right if len(features_right) > 0 else 'nan')
					inside.append(features_inside if len(features_inside) > 0 else 'nan')
				else:
					left_bp.append('nan')
					right_bp.append('nan')
					inside.append('nan')
			df['OVERLAPS-LEFT-BP_' + column_name] = left_bp
			df['OVERLAPS-RIGHT-BP_' + column_name] = right_bp
			df['INSIDE-VAR-BP_' + column_name] = inside
		else:
			print(filename)
			assert(False)
	df.to_csv(args.output, sep='\t', index=False, na_rep='nan')
