# README #

Scripts and Pipelines for LRS Project.

## Input files ##

* BAM-files: must be put into a folder called `` data/input ``. The corresponding names must be listed in the config file (under: file_names)
* annotation BED files: BED files must be located in folder `` annotations/ `` and listed in the config file. There are two categories of BED-files, such containing variant coordinates and others that contain annotation regions. Names of BED files must be specified under annotation_variants or annotation_regions.
* reference file: path to reference FASTA must be provided in the config file.



## Outputs ##

* plots: a folder called ``graphics/ `` is produced that contains all plots
* tables: tables with annotations for each BAM file are produced and can be found under: `` data/intersections/<bam-file-name>/<bam-file-name>_callset_statistics.tsv ``
* details on the columns of the final table can be found here: https://docs.google.com/presentation/d/1ADoYQna9vRhjBgn2MAKEyqIYVAU7KY9j-_BWr2HWqq8/edit?usp=sharing

## Running pipeline ##

``  snakemake --use-conda -j <number of cores>  ``   

Note: the rule that downloads the HGSVC data required internet access and thus does not work on our HPC. The corresponding VCFs can be downloaded manually from:
http://ftp.1000genomes.ebi.ac.uk/vol1/ftp/data_collections/HGSVC2/release/v2.0/integrated_callset/ 

## Notes ##
The file 210113_finale_Genliste_hg38_Update1_furJana_LRS-fixed.tsv was provided by Rabea. The exons were downloaded from https://www.ensembl.org/biomart/martview/ (select human hg38, Attributes: Structures). Other annotation files were provided by Dani.
