# README #

Annotation pipeline used for LRS Project.

## Input files ##

* callset VCFs: must be put into a folder called `` input/ ``. Files must be named following this pattern: `` input/<caller_name>/<sample_name>.vcf ``. The list of callers and samples must also be provided in the config file (under: `` callers `` and `` file_names ``, respectively). For each sample, there must be a callset for each listed caller.
* annotation BEDs: are provided already in folder `` ../annotations/ `` and specified in the config file. No changes required here, unless you want to add additional annotation files.

## Outputs ##

* annotation tables: a table with annotations for each sample can be found under: `` data/cleaned-up-tables/<sample_name>.tsv ``
* details on the columns of the final table can be found here: https://docs.google.com/presentation/d/1ADoYQna9vRhjBgn2MAKEyqIYVAU7KY9j-_BWr2HWqq8/edit?usp=sharing
* plots: if more than one caller was used or more than one sample were provided, some plots will be generated in folder: ``graphics/ ``

## Running pipeline ##

``  snakemake --use-conda -j <number of cores>  ``   

Note: the rule that downloads the HGSVC data required internet access and thus does not work on our HPC. The corresponding files can be downloaded manually from:
http://ftp.1000genomes.ebi.ac.uk/vol1/ftp/data_collections/HGSVC2/release/v2.0/integrated_callset/:

* Store the VCF (http://ftp.1000genomes.ebi.ac.uk/vol1/ftp/data_collections/HGSVC2/release/v2.0/integrated_callset/variants_freeze4_sv_insdel_alt.vcf.gz) as an uncompressed VCF named: `` data/downloaded/hgsvc.vcf ``
* the TSV file (http://ftp.1000genomes.ebi.ac.uk/vol1/ftp/data_collections/HGSVC2/release/v2.0/integrated_callset/variants_freeze4_sv_insdel.tsv.gz) as `` data/downloaded/hgsvc.tsv.gz ``
