import sys
import argparse


def parse_table(filename):
	lines = []
	for line in open(filename, 'r'):
		fields = line.split()
		lines.append(fields)
	return lines
	
def same_length(l1,l2):
	"""
	Check that l1 is at most 1.5 x as large as l2, and
	at least 0.5x as large as l2
	"""
	# if variant length given in annotation is 0, keep entry
	# (sometimes) insertion have length 0 in their annotations
	# since length of the reference interval was measured apparently..
	if l1 == 0:
		return True
	size_diff = l1 / l2
	return (0.5 < size_diff < 1.5)


def same_type(vartype, entry):
	"""
	Check if variant type matches variant
	type of the entry.
	"""
	if vartype == "INS":
		return ("gain_" in entry) or ("insertion_" in entry) or ("gain+loss_" in entry)
	elif vartype == "DEL":
		return ("loss_" in entry) or ("deletion_" in entry) or ("gain+loss_" in entry)
	elif vartype == "DUP" or vartype == "DUP:TANDEM":
		return ("gain_" in entry) or ("insertion_" in entry) or ("gain+loss_" in entry) or ("duplication_" in entry) or ("tandem-duplication_" in entry)
	else:
		return True


def filter_length(table, colname):
	"""
	In the specified column, filter out all entries if the length
	encoded in the entry deviates to much from the variant length.
	This assumes that in the entries look like this:
	nssv16166810_ALL_AF_0.578224_CNV_16751_ 
	"""
	# index of the column
	assert 'VARIANT-OVERLAP_' + colname in table[0]
	index = table[0].index('VARIANT-OVERLAP_' + colname)
	index_len = table[0].index('length')
	sys.stderr.write(str(index))
	for i in range(1, len(table)):
		new_field = []
		varlength = int(table[i][index_len])
		for entry in table[i][index].split(';'):
			if entry == "nan":
				new_field = [entry]
				break
			length = int(entry.split('_')[-2])
			keep_entry = same_length(length, varlength)
			if keep_entry:
				new_field.append(entry)
		if not new_field:
			new_field = ["nan"]
		table[i][index] = ';'.join(new_field)


def compute_frequencies(total, gains, losses):
	if total == 'nan':
		return 'nan','nan'
	gains_freq = 'nan' if gains == 'nan' else float(gains) / float(total)
	losses_freq = 'nan' if losses == 'nan' else float(losses) / float(total)
	return gains_freq, losses_freq


def filter_gain_loss(table, colname):
	"""
	For deletions, keep only entries marked as deletion, loss, gain+loss
	For insertions, keep: insertion, gain
	For duplications, keep: insertion, gain
	"""

	assert 'VARIANT-OVERLAP_' + colname in table[0]
	index = table[0].index('VARIANT-OVERLAP_' + colname)
	index_type = table[0].index('var_type')
	index_len = table[0].index('length')
	sys.stderr.write(str(index))
	table[0].append('GAINS_' + colname)
	table[0].append('LOSSES_' + colname)
	for i in range(1, len(table)):
		new_field = []
		gains_freqs = []
		losses_freqs = []
		vartype = table[i][index_type]
		varlength = int(table[i][index_len])
		for entry in table[i][index].split(';'):
			if entry == "nan":
				new_field = [entry]
				gains_freqs = ['nan']
				losses_freqs = ['nan']
				break
			# keep only entries with similar type
			keep_type = same_type(vartype, entry)
			# keep only entries with similar length
			length = int(entry.split('_')[1])
			keep_length = same_length(length, varlength)
			# compute gains/losses
			size = entry.strip().split('_')[-3]
			gains = entry.strip().split('_')[-2]
			losses = entry.strip().split('_')[-1]
			gains_freq, losses_freq = compute_frequencies(size, gains, losses)
			if keep_length and keep_type:
				new_field.append(entry)
				gains_freqs.append(str(gains_freq))
				losses_freqs.append(str(losses_freq))
		if not new_field:
			new_field = ["nan"]
			gains_freqs = ["nan"]
			losses_freqs = ["nan"]
		table[i][index] = ';'.join(new_field)
		table[i].append(';'.join(gains_freqs))
		table[i].append(';'.join(losses_freqs))
	
	



def polish_table(filename, length=None, type=None):
	table = parse_table(filename)

	# filter out entries with too different length
	if length:
		for colname in length:
			filter_length(table, colname)
			
	# filter out entries with different types
	if type:
		for colname in type:
			filter_gain_loss(table, colname)
	# print table
	for line in table:
		print('\t'.join(line))



if __name__ == '__main__':
	parser = argparse.ArgumentParser(prog='polish-table.py', description=__doc__)
	parser.add_argument('-t', '--table', required=True, help='Table to be considered.')
	parser.add_argument('-l', '--length', nargs= '+', help="Columns to consider for length filtering. Only keep entry if length is similar to the variant length.")
	parser.add_argument('-v', '--type', nargs= '+', help="Columns to consider for type filtering. Only keep entry if type matches.")
	args = parser.parse_args()
	

	polish_table(args.table, args.length if args.length else None, args.type if args.type else None)


