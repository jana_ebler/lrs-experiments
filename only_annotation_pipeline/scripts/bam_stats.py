import sys
import argparse
from collections import defaultdict
import numpy as np
import matplotlib.pyplot as plt

def compute_median(histogram, total, limit):
	"""
	Computes the median.
	"""
	medians = []
	index = 0
	middle = [ total/2] if total % 2 != 0 else [ (total) // 2, (total+1) // 2 ]
	set_first = False
	set_second = False
	for bin in sorted(histogram.keys()):
		count = histogram[bin]
		index += count
		if (index >= middle[0]) and (not set_first):
			medians.append(bin)
			set_first = True
			if len(middle) == 1:
				set_second = True
				break
			continue
		if (index >= middle[0]) and (len(middle) > 1) and (not set_second):
			medians.append(bin)
			set_second = True
			break
		if set_first and set_second:
			break
	return sum(medians) / len(medians)
		

def compute_statistics(outname, limit):
	"""
	Compute read length distirbution and some
	BAM file statistics
	"""
	bins = defaultdict(lambda: 0)
	total_alignments = 0
	total_reads = set([])
	max_len = 0
	min_len = float('inf')
	mean_len = 0
	for line in sys.stdin:
		fields = line.split()
		assert len(fields) == 2
		readlen = len(fields[1].strip())
		readname = fields[0]
		total_reads.add(readname)
		bins[readlen] += 1
		max_len = max(readlen, max_len)
		min_len = min(readlen, min_len)
		mean_len += readlen
		total_alignments += 1
	# compute median
	median = compute_median(bins, total_alignments, max_len)
	mean_len /= float(total_alignments)
	plt.hist(bins.keys(), weights=bins.values(), bins=range(0,limit,999))
	plt.axvline(median, color='r', linestyle='dashed', linewidth=1)
	plt.xticks(range(0,limit,5000), rotation=90)
	plt.xlabel('read length (bp)')
	plt.ylabel('count')
	plt.tight_layout()
	plt.savefig(outname + '.pdf')
	
	with open(outname + '.tsv', 'w') as outtable:
		outtable.write('\t'.join(['n_reads', 'n_alignments', 'min_readlen', 'max_readlen', 'mean_readlen', 'median_readlen']) + '\n')
		outtable.write('\t'.join([str(len(total_reads)), str(total_alignments), str(min_len), str(max_len), str(mean_len), str(median)]) + '\n')
		

if __name__ == '__main__':
	parser = argparse.ArgumentParser(prog='| python bam-stats.py -outfile <outfile name> ', description=__doc__)
	parser.add_argument('-outfile', metavar='outfile', required=True, help='name of the output file.')
	args = parser.parse_args()

	compute_statistics(args.outfile, 50000)
