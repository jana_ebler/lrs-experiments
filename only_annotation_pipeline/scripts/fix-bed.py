import sys

for line in sys.stdin:
	fields = line.strip().split()
	if len(fields) < 4:
		# assign a ID
		feature_id = fields[0] + ':' + fields[1] + '-' + fields[2]
		fields.append(feature_id)
	print('\t'.join(fields))
