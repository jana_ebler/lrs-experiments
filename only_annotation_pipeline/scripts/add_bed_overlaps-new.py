import sys, argparse
from collections import defaultdict
import pandas as pd

transcript_to_gene = {}

if __name__ == '__main__':
	parser = argparse.ArgumentParser(prog='add_bed_overlaps.py', description=__doc__)
	parser.add_argument('-t', '--table', required=True, help='Table to which annotations shall be added.')
	parser.add_argument('-i', '--intersections', required=True, nargs='+', help='bedtools intersect output for two VCF files (keeping both entries)')
	parser.add_argument('-o', '--output', required=True, help='output table with added columns.')
	parser.add_argument('-a', '--annotations', required=True, help='genocode annotations')
#	parser.add_argument('-n', '--name', default='intersection', nargs='+', help='name of the callset that was intersected.')
	args = parser.parse_args()

	for line in open(args.annotations, 'r'):
		if line.startswith('#'):
			continue
		fields = line.split('\t')
		assert len(fields) == 8
		infos = {i.strip().split()[0] : i.strip().split()[1] for i in fields[8].split(';')}
		assert "gene_id" in infos
		assert "gene_name" in infos
		if not "transcript_id" in infos:
			continue
		assert "transcript_id" in trios
		transcript_id = infos["transcript_id"][1:-1]
		gene_name = infos["gene_name"][1:-1]
		gene_id = infos["gene_id"][1:-1]
		assert
		transcript_to_gene[transcript_id] = [gene_name, gene_id]
	print(transcript_to_gene)

		

	df = pd.read_csv(args.table, sep='\t')
	# add table indicating overlap
	for filename in args.intersections:
		column_name = '_'.join(filename.split('/')[-1][:-4].split('_')[1:])
		callset_name = filename.split('/')[-1][:-4]
		if callset_name.startswith('closest-'):
			intersections = defaultdict(list)
			for line in open(filename, 'r'):
				fields = line.split('\t')
				id_first = fields[2].strip()
				id_second = fields[11].strip()
				transcript_id = id_second.split('_')
				if transcript_id startswith('ENST') and transcript_id in transcript_to_gene:
					id_second = '_'.join([transcript_to_gene[transcript_id][0], transcript_to_gene[transcript_id][1], transcript_id])
				distance = fields[-1].strip()
				if id_second != ".":
					intersections[id_first].append(id_second + '(' + distance + 'bp)')
			closest = []
			for var in df['ID']:
				if var in intersections:
					feature = ','.join(list(set(intersections[var])))
					assert len(feature) > 1
					closest.append(','.join(list(set(intersections[var]))))
				else:
					closest.append('nan')
			if 'downstream' in filename:
				direction = 'DOWNSTREAM'
			else:
				assert 'upstream' in filename
				direction = 'UPSTREAM'
			df['CLOSEST-' + direction + '_' + column_name] = closest
		elif callset_name.startswith('variants_'):
			intersections = defaultdict(list)
			for line in open(filename, 'r'):
				fields = line.split('\t')
				id_first = fields[2].strip()
				id_second = fields[11].strip()
				assert id_second != '.'
				intersections[id_first].append(id_second)
			variants = []
			for var in df['ID']:
				if var in intersections:
					feature = ','.join(list(set(intersections[var])))
					assert len(feature) > 1
					variants.append(feature)
				else:
					variants.append('nan')
			df['VARIANT-OVERLAP_' + column_name] = variants
		elif callset_name.startswith('regions_'):
			intersections = defaultdict(list)
			for line in open(filename, 'r'):
				fields = line.split('\t')
				start_first = int(fields[1])
				info_fields = {d.split('=')[0] : d.split('=')[1] for d in fields[7].split(';') if '=' in d}
				end_first = int(info_fields['END'])
				id_first = fields[2].strip()
				start_second = int(fields[9])
				end_second = int(fields[10])
				id_second = fields[11].strip()
				assert id_second != '.'
				overlaps_left = False
				overlaps_right = False
				inside_variant = False
				if (start_second <= start_first):
					overlaps_left = True
				if (end_second >= end_first):
					overlaps_right = True
				if (start_second > start_first) and (end_second < end_first):
					inside_variant = True
				assert overlaps_left or overlaps_right or inside_variant
				intersections[id_first].append([id_second, overlaps_left, overlaps_right, inside_variant])
			left_bp = []
			right_bp = []
			inside = []
			for var in df['ID']:
				if var in intersections:
					assert len(intersections[var]) > 0
					features_left = ','.join(list(set([stats[0] for stats in intersections[var] if stats[1]])))
					features_right = ','.join(list(set([stats[0] for stats in intersections[var] if stats[2]])))
					features_inside = ','.join(list(set([stats[0] for stats in intersections[var] if stats[3]])))
				
					left_bp.append(features_left if len(features_left) > 0 else 'nan')
					right_bp.append(features_right if len(features_right) > 0 else 'nan')
					inside.append(features_inside if len(features_inside) > 0 else 'nan')
				else:
					left_bp.append('nan')
					right_bp.append('nan')
					inside.append('nan')
			df['OVERLAPS-LEFT-BP_' + column_name] = left_bp
			df['OVERLAPS-RIGHT-BP_' + column_name] = right_bp
			df['INSIDE-VAR-BP_' + column_name] = inside
		else:
			print(filename)
			assert(False)
	df.to_csv(args.output, sep='\t', index=False, na_rep='nan')
