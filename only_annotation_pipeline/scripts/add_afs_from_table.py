import sys

table = sys.argv[1]
colname = sys.argv[2]

pos = None
id_to_af = {}
for line in sys.stdin:
	fields = line.split('\t')
	if line.startswith('ID'):
		assert 'POP_ALL_AF' in fields
		pos = fields.index('POP_ALL_AF')
		continue
	assert pos is not None
	var_id = fields[0].strip()
	var_af = fields[pos]
	id_to_af[var_id] = var_af

# add afs to table
pos = None
for line in open(table, 'r'):
	fields = line.split()
	if line.startswith('ID'):
		assert 'ID_'+ colname in fields
		pos = fields.index('ID_' + colname)
		print(line.strip() + '\t' + 'AF_' + colname)
		continue
	assert pos is not None
	var_ids = fields[pos].strip()
	var_afs = []
	for var_id in var_ids.split(';'):
		var_af = 'nan'
		if var_id in id_to_af:
			var_af = id_to_af[var_id]
		var_afs.append(var_af)
	fields.append(';'.join(var_afs))
	print('\t'.join(fields))
