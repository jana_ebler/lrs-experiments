import sys

genes = sys.argv[1]
exons = sys.argv[2]

gene_names = []
chromosomes = [str(i) for i in range(1,23)] + ['X', 'Y']

# read gene names
for line in open(genes, 'r'):
	fields = line.split()
	gene_names.append(fields[0].strip())


for line in open(exons, 'r'):
	fields = line.split()
	if not fields[4] in gene_names:
		continue
	chrom = fields[6]
	if not chrom in chromosomes:
		continue
	start = fields[2]
	end = fields[3]
	gene_name = fields[4]
	exon_name = fields[5]
	print('\t'.join(['chr' + chrom, start, end, gene_name, exon_name]))
