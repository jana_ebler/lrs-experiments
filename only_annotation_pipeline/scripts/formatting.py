import sys
import pandas as pd
from collections import defaultdict

instructions_file = sys.argv[1]
table_file = sys.argv[2]
sample_name = sys.argv[3]
outname = sys.argv[4]
callers = [c for c in sys.argv[5].strip().split(',')]

old_to_new = {}
new_order = []
with open(instructions_file) as infile:
	instructions = [ [f for f in next(infile).strip().split('\t')] for x in range(2)]
	print(instructions)
	new_order = []
	for new,old in zip(instructions[0], instructions[1]):
		if '_filtered' in old:
			continue
		if new == "":
			continue
		if old.startswith('in_'):
			continue
		if old.startswith('ID_'):
			continue
		new_order.append(new)
		old_to_new[old] = new


for caller in callers:
	print(caller)
	old_to_new['in_' + caller + '_' + sample_name] = caller
	old_to_new['ID_' + caller + '_' + sample_name] = 'ID_' + caller
	new_order.append(caller)
	new_order.append('ID_' + caller)
old_to_new['in_hgsvc'] = 'in_HGSVC'
old_to_new['ID_hgsvc'] = 'ID_HGSVC'
new_order.append('in_HGSVC')
new_order.append('ID_HGSVC')


# read table
df = pd.read_csv(table_file, sep='\t')
print(df)

# rename columns
df.rename(columns=old_to_new, inplace=True)
print(df)

# keep only columns in old_to_new
df.to_csv(outname, columns=new_order, sep='\t', index=False, na_rep='nan')
